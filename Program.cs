﻿using System;

namespace comp5002_10006663_assessment
{
    class Program
    {
        static void Main(string[] args)
        {
           
            Console.WriteLine("Hello. Welcome to my shop");

            var username = " ";
            Double num1 = 0;
            Double num2 = 0;
            Double sum = 0;
            var answer = "yes";
            Double GST = 1.15;
            var Total = 0.0;

            Console.WriteLine("What is your name?");
            username = Console.ReadLine();
            Console.WriteLine("Hello " + username);

            Console.WriteLine("Please enter a number with 2 decimal places e.g. 2.68");
            num1 = Double.Parse(Console.ReadLine());

            Console.WriteLine(" Do you want to add another number (yes/no)");
            answer = (Console.ReadLine());
            if (answer == "yes")
            {
                Console.WriteLine("Enter another number with 2 decimal places");
                num2 = Double.Parse(Console.ReadLine());
            }
            else
            { 
                num2 = 0;
            }

            sum = num1 + num2;
            Total = sum * GST;
            Total = Math.Round(Total, 2);

            Console.WriteLine("Your total price is " + sum);

            Console.WriteLine("Your total, including GST, is " + Total);

            Console.WriteLine("Thank you for shopping with us, please come again");

            /*
             Pseudo Code
             Display “Welcome to my shop”
             Declare variables - username, num1, num2, answer, sum, GST, Total
             Ask user’s name
             Store user input as ‘username’
             Ask user for a number with 2 decimal places
             Set user input as num1
             Ask user if they want to add another number
             Create “ if ” statement

             If ‘answer’ = “yes”
                         Ask user for another number with 2 decimal places
                         Set user input as ‘num2’
             else
                         num2 = 0
  
             Sum = num1+num2    
             Calculate ‘GST’ + ‘sum’
             Store as ‘total’
             Round total to 2 decimal places
             Display “Your total is {sum}”
             Display “Your total, including GST, is {total}
             Display “Thank you for shopping with us, please come again”            
             */
        }
    }
}
